from distutils.util import strtobool


def str_to_bool(string):
    # True values are:  y, yes, t, true, on and 1
    # False values are: n, no, f, false, off and 0
    return bool(strtobool(string))