from setup import configs

from arduino.monitoring import Monitoring
from services.controller import Controller

if __name__ == '__main__':

    monitoring = Monitoring()
    monitoring.setDaemon(False)
    monitoring.start()

    controller = Controller()
    controller.setDaemon(True)
    controller.start()