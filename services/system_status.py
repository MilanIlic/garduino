from storage.database import Database

import logging
import json
from flask_restful import Resource

logging.basicConfig()
logger = logging.getLogger('system_status')
logger.setLevel(logging.DEBUG)

'''
    API point: /status
    Retrieves last scraped metrics from Arudino
'''
class SystemStatus(Resource):
    def get(self):

        monitoring_results = self.get_latest_monitoring_stats()
        latest_watering = self.get_latest_watering_stats()

        results = {
            "air_temperature": monitoring_results.get("air_temperature", "unknown"),
            "air_humidity": monitoring_results.get("air_humidity", "unknown"),
            "soil_moisture": monitoring_results.get("soil_moisture", "unknown"),
            "timestamp": monitoring_results.get("timestamp", "unknown"),
            "watering_done_at": latest_watering
        }

        return results

    def get_latest_monitoring_stats(self):
        db = Database.getInstance()
        results = {}

        command = "SELECT * " \
                  "FROM monitoring " \
                  "ORDER BY id DESC " \
                  "LIMIT 1"

        db.query(command)
        monitoring_results = db.fetch_result()

        if monitoring_results is not None and len(monitoring_results) > 0:
            monitoring_results = monitoring_results[0]
            results = {
                "air_temperature": str(monitoring_results[1]),
                "air_humidity": str(monitoring_results[2]),
                "soil_moisture": str(monitoring_results[3]),
                "timestamp": str(monitoring_results[4])
            }

            logger.debug("Monitoring data: " + json.dumps(results, indent=2))

        return results

    def get_latest_watering_stats(self):
        db = Database.getInstance()

        command = "SELECT watering_done_at " \
                  "FROM watering " \
                  "ORDER BY watering_done_at DESC " \
                  "LIMIT 1"

        db.query(command)
        watering_results = db.fetch_result()

        if watering_results is not None and len(watering_results) > 0:
            return str(watering_results[0][0])
        else:
            return "unknown"
