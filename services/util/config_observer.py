from abc import abstractmethod


class ConfigObserver:
    """
    The Observer interface declares the update method, used by subjects.
    """

    @abstractmethod
    def update(self, subject=None):
        """
        Receive update from subject.
        """
        pass
