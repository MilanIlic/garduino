from services.system_config import SystemConfig
from services.watering.watering_checker_controller import WateringCheckerController
from services.watering_api import WateringApi
from services.system_status import SystemStatus

from setup import configs

import logging
import threading

from utils.functions import str_to_bool

logging.basicConfig()
logger = logging.getLogger('controller')
logger.setLevel(logging.DEBUG)


class Controller (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        configs.api.add_resource(SystemStatus, "/status")

        SystemConfig.create_system_config_table()
        configs.api.add_resource(SystemConfig, "/config")

        configs.api.add_resource(WateringApi, "/water-plant")

        # Start auto watering process
        auto_watering_enabled = str_to_bool(configs.config.get("auto_watering"))
        if auto_watering_enabled:
            WateringCheckerController.getInstance().startWateringChecker()

        service_port = configs.config.get("service_port", 80)
        configs.app.run(debug=True, host='0.0.0.0', port=service_port, use_reloader=False)