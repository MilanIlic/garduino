from storage.database import Database
from arduino.communication.serial_communicator import SerialCommunicator
from setup import configs

import datetime
import functools
import threading
import logging
from flask import make_response

logging.basicConfig()
logger = logging.getLogger('watering-controller')
logger.setLevel(logging.DEBUG)

def synchronized(wrapped):
    lock = threading.Lock()
    @functools.wraps(wrapped)
    def _wrap(*args, **kwargs):
        with lock:
            return wrapped(*args, **kwargs)
    return _wrap

'''
    Sends appropriate command for watering to Arduino.
    Updates last watering timestamp in DB.
'''
class WateringController:
    __instance = None

    @staticmethod
    def getInstance():
        if WateringController.__instance == None:
            WateringController()
        return WateringController.__instance

    def __initController(self):
        self.db = Database.getInstance()
        self.arduino = SerialCommunicator.getInstance()
        self.create_watering_table()

    def __init__(self):
        if WateringController.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            self.__initController()
            WateringController.__instance = self

    def create_watering_table(self):
        ''' Creates watering table in the PostgreSQL database '''

        command = "CREATE TABLE IF NOT EXISTS watering ( \
                   watering_done_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'), \
                   light_on_during_watering bool, \
                   comment text \
                   )"

        self.db.query(command)


    @synchronized
    def do_watering(self, led_on = False, comment = ""):

        try:
            # Commands
            # WATER 1
            # WATER-LED 2

            if (led_on):
                logger.debug("Watering with LED lamp.")
                self.arduino.write(configs.ARDUINO_WATER_LED) # water-led for turrning on LED while watering
            else:
                logger.debug("Sending instruction for watering to Arduino.")
                self.arduino.write(configs.ARDUINO_WATER)

            command = "INSERT INTO watering(light_on_during_watering, comment) VALUES ({0},'{1}')".format(led_on, comment)

            self.db.query(command)
            logger.debug("Watering done.")

            return make_response({"status": "DONE"}, 200)
        except Exception as e:
            logger.error("Error while communicating with Arduino: " + str(e))
            return make_response({"status": "FAILED"}, 500)
