from services.watering_controller import WateringController
from setup import configs
from notifier.telegram import TelegramNotifier

import logging
from flask_restful import Resource

from utils.functions import str_to_bool

logging.basicConfig()
logger = logging.getLogger('watering_api')
logger.setLevel(logging.DEBUG)

'''
    API point: GET /water-plant
    API to start watering without any conditions.
'''
class WateringApi(Resource):

    def __init__(self):
        self.wateringController = WateringController.getInstance()

    def get(self):
        logger.debug("Plant watering...")
        led_on = str_to_bool(configs.config.get("light_on_during_watering"))
        send_notification = str_to_bool(configs.config.get("telegram_notifier_enabled"))

        if send_notification:
            TelegramNotifier.getInstance().send_text("Watering requested. Watering the plant...")

        return self.wateringController.do_watering(led_on = led_on, comment="Watering initiated by user")



