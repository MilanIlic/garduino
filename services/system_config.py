import json
import logging

from flask import make_response
from flask_restful import Resource, request, reqparse

from setup import configs
from storage.database import Database

logging.basicConfig()
logger = logging.getLogger('system_config')
logger.setLevel(logging.DEBUG)

'''
    API poing: /config
    Dynamic system configuration allows changing of some system parameters and thresholds via API point.
    When any parameter is changed, 'system_config' table gets updated.
    Supported params:
     * auto_watering
     * light_on_during_watering
     * telegram_notifier_enabled
     * watering_scrape_interval
     * watering_num_of_scrapes
     * watering_moisture_threshold
     * watering_temperature_threshold
     * watering_humidity_threshold
'''

class SystemConfig(Resource):

    def __init__(self):
        self.db = Database.getInstance()
        self.parser = reqparse.RequestParser()

    """
    List of subscribers
    """
    __observers = []

    @staticmethod
    def attach(observer):
        print("SystemConfig: Attached an observer.")
        SystemConfig.__observers.append(observer)

    @staticmethod
    def detach(observer):
        SystemConfig.__observers.remove(observer)

    @staticmethod
    def notify():
        """
        Trigger an update in each subscriber.
        """

        logger.debug("SystemConfig: Notifying config observers...")
        for observer in SystemConfig.__observers:
            logger.debug("SystemConfig: Notifying observer...")
            observer.update()

    def get(self):

        command = "SELECT " \
                  "auto_watering, " \
                  "light_on_during_watering, " \
                  "telegram_notifier_enabled," \
                  "watering_scrape_interval, " \
                  "watering_num_of_scrapes, " \
                  "watering_moisture_threshold, " \
                  "watering_temperature_threshold, " \
                  "watering_humidity_threshold " \
                  "FROM system_config"

        self.db.query(command)
        query_results = self.db.fetch_result()
        if query_results is not None and len(query_results) > 0:
            query_results = query_results[0]

        # Example output:
        # results = {
        #     "auto_watering": str(query_results[0]),
        # }

        logger.debug("Configs from DB: " + json.dumps(query_results, indent=2))


        result = {
            "auto_watering": str(query_results[0]),
            "light_on_during_watering": str(query_results[1]),
            "telegram_notifier_enabled": str(query_results[2]),
            "watering_scrape_interval": str(query_results[3]),
            "watering_num_of_scrapes": str(query_results[4]),
            "watering_moisture_threshold": str(query_results[5]),
            "watering_temperature_threshold": str(query_results[6]),
            "watering_humidity_threshold": str(query_results[7]),
        }

        logger.debug("Current config: " + json.dumps(result, indent=2))

        return result

    def put(self):

        self.parser.add_argument('auto_watering', type=str)
        self.parser.add_argument('light_on_during_watering', type=str)
        self.parser.add_argument('telegram_notifier_enabled', type=str)
        self.parser.add_argument('watering_scrape_interval', type=str)
        self.parser.add_argument('watering_num_of_scrapes', type=str)
        self.parser.add_argument('watering_moisture_threshold', type=str)
        self.parser.add_argument('watering_temperature_threshold', type=str)
        self.parser.add_argument('watering_humidity_threshold', type=str)

        args = self.parser.parse_args(strict=True, http_error_code=404)

        logger.debug("Passed args: " + json.dumps(args, indent=4))


        try:
            # auto_watering
            new_auto_watering = args.get("auto_watering")

            # light_on_during_watering
            new_light_on_during_watering = args.get("light_on_during_watering")

            # telegram_notifier_enabled
            new_telegram_notifier_enabled = args.get("telegram_notifier_enabled")

            # watering_scrape_interval
            new_watering_scrape_interval = int(args.get("watering_scrape_interval"))

            # watering_num_of_scrapes
            new_watering_num_of_scrapes = int(args.get("watering_num_of_scrapes"))

            # watering_moisture_threshold
            new_watering_moisture_threshold = int(args.get("watering_moisture_threshold"))

            # watering_temperature_threshold
            new_watering_temperature_threshold = int(args.get("watering_temperature_threshold"))

            # watering_humidity_threshold
            new_watering_humidity_threshold = int(args.get("watering_humidity_threshold"))
        except ValueError as e:
            # Handle the exception
            logger.error("Parsing failure during config update " + str(e))
            return make_response("Parsing error.", 404)

        # Update GLOBAL config
        configs.config.update(args)

        # Notify subscribers
        SystemConfig.notify()

        logger.debug("New config: " + json.dumps(configs.config, indent=2))

        # UPDATE values in DB
        command = "UPDATE system_config SET " \
                  "auto_watering={0}," \
                  "light_on_during_watering={1}," \
                  "telegram_notifier_enabled={2}," \
                  "watering_scrape_interval={3}," \
                  "watering_num_of_scrapes={4}," \
                  "watering_moisture_threshold={5}," \
                  "watering_temperature_threshold={6}," \
                  "watering_humidity_threshold={7}" \
            .format(new_auto_watering,
                    new_light_on_during_watering,
                    new_telegram_notifier_enabled,
                    new_watering_scrape_interval,
                    new_watering_num_of_scrapes,
                    new_watering_moisture_threshold,
                    new_watering_temperature_threshold,
                    new_watering_humidity_threshold)

        logger.debug("UPDATE COMMAND: {0}".format(command))

        try:
            self.db.query(command)
        except ValueError as e:
            # Handle the exception
            logger.error("Database failure during config update " + str(e))
            return make_response("Configuration update failed.", 500)

        return make_response({}, 200)

    @staticmethod
    def create_system_config_table():
        ''' Creates system_config table in the PostgreSQL database '''

        db = Database.getInstance()

        command = 'CREATE TABLE IF NOT EXISTS system_config ( \
                    auto_watering bool, \
                    light_on_during_watering bool, \
                    telegram_notifier_enabled bool, \
                    watering_scrape_interval smallint, \
                    watering_num_of_scrapes smallint, \
                    watering_moisture_threshold smallint, \
                    watering_temperature_threshold smallint, \
                    watering_humidity_threshold smallint \
        )'

        db.query(command)

        # Check for values
        command = "SELECT auto_watering, \
                  light_on_during_watering, \
                  telegram_notifier_enabled, \
                  watering_scrape_interval, \
                  watering_num_of_scrapes, \
                  watering_moisture_threshold, \
                  watering_temperature_threshold, \
                  watering_humidity_threshold FROM system_config"

        db.query(command)
        query_results = db.fetch_result()
        if len(query_results) == 0:
            logger.debug("system_config table is empty. Inserting init values from config file.")
            SystemConfig.insert_init_values(db)
        else:
            # there are config values in DB so we need to load them
            logger.debug("system_config table contains parameters values. Loading config from the database.")
            SystemConfig.load_init_values(query_results[0])

    @staticmethod
    def insert_init_values(db):
        # Insert init values in DB
        command = "INSERT INTO system_config( \
                  auto_watering, \
                  light_on_during_watering, \
                  telegram_notifier_enabled, \
                  watering_scrape_interval, \
                  watering_num_of_scrapes, \
                  watering_moisture_threshold, \
                  watering_temperature_threshold, \
                  watering_humidity_threshold) VALUES({0},{1},{2},{3},{4},{5},{6},{7})" \
            .format(configs.config.get("auto_watering"),
                    configs.config.get("light_on_during_watering"),
                    configs.config.get("telegram_notifier_enabled"),
                    configs.config.get("watering_scrape_interval"),
                    configs.config.get("watering_num_of_scrapes"),
                    configs.config.get("watering_moisture_threshold"),
                    configs.config.get("watering_temperature_threshold"),
                    configs.config.get("watering_humidity_threshold")
                    )

        db.query(command)

    @staticmethod
    def load_init_values(config_values):
        # Update config params with values from DB

        result = {
            "auto_watering": str(config_values[0]),
            "light_on_during_watering": str(config_values[1]),
            "telegram_notifier_enabled": str(config_values[2]),
            "watering_scrape_interval": str(config_values[3]),
            "watering_num_of_scrapes": str(config_values[4]),
            "watering_moisture_threshold": str(config_values[5]),
            "watering_temperature_threshold": str(config_values[6]),
            "watering_humidity_threshold": str(config_values[7]),
        }

        # Update GLOBAL config
        configs.config.update(result)

        # Notify subscribers
        SystemConfig.notify()

        logger.debug("New loaded config: " + json.dumps(configs.config, indent=2))
