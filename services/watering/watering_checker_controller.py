from services.system_config import SystemConfig
from setup import configs
from services.watering.watering_checker import WateringChecker
from services.util.config_observer import ConfigObserver

import logging

from utils.functions import str_to_bool

logging.basicConfig()
logger = logging.getLogger('watering-checker-control')
logger.setLevel(logging.DEBUG)


'''
    Configures, starts and stops WateringChecker threads
'''
class WateringCheckerController(ConfigObserver):
    __instance = None

    @staticmethod
    def getInstance():
        if WateringCheckerController.__instance == None:
            WateringCheckerController()
        return WateringCheckerController.__instance

    def __init__(self):
        if WateringCheckerController.__instance != None:
            raise Exception("Class is a singleton!")
        else:
            self.initParams()
            WateringCheckerController.__instance = self

    def initParams(self):
        self.auto_watering_thread = None
        SystemConfig.attach(self)

    def __del__(self):
        self.stopWateringChecker()

    def update(self, subject=None):
        """
            Config is changed. Start/Stop watering checker thread.
        """
        logger.debug("Config update")
        auto_watering_enabled = str_to_bool(configs.config.get("auto_watering"))

        if auto_watering_enabled:
            self.startWateringChecker()
        else:
            self.stopWateringChecker()

        pass

    def startWateringChecker(self):
        if self.auto_watering_thread is None or self.auto_watering_thread.isAlive() == False:
            logger.debug("Starting Watering Checker")
            self.auto_watering_thread = WateringChecker()
            self.auto_watering_thread.start()

    def stopWateringChecker(self):
        if self.auto_watering_thread is not None and self.auto_watering_thread.isAlive():
            logger.debug("Stopping Watering Checker")
            self.auto_watering_thread.stop()