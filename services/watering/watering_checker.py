import threading
import logging
import time

from setup import configs
from storage.database import Database
from notifier.telegram import TelegramNotifier
from services.watering_controller import WateringController
from utils.functions import str_to_bool

logging.basicConfig()
logger = logging.getLogger('watering-checker')
logger.setLevel(logging.DEBUG)

'''
    Thread which queries DB for last metrics and decides if conditions for watering are met.
      
    If any of the conditions is met:
      * watering command will be sent via WateringController
      * notification to user will be sent via TelegramNotifier
'''

class WateringChecker(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self._stop_event = threading.Event()
        self.db = Database.getInstance()
        self.wateringController = WateringController.getInstance()

    def stop(self):
        self._stop_event.set()

    def should_water(self):
        '''
        Configurable variables:
              * check_interval: How much seconds to wait before next check
              * num_of_scrapes: How many last scrapes should be taken in consideration

              * moisture_threshold: Water the plant if the soil moisture percentage is BELOW the threshold. ( -1 means ignore metric )
              * temeperature_threshold: Water the plant if the temperature is ABOVE the threshold. ( -1 means ignore metric )
              * humidity_threshold: Water the plant if the air humidity percentage is BELOW the threshold. ( -1 means ignore metric )

        :return: bool, weather to do watering
        '''

        num_of_scrapes = int(configs.config.get("watering_num_of_scrapes"))
        moisture_threshold = int(configs.config.get("watering_moisture_threshold", -1)) # -1 means that the metric shouldn't be taken in consideration
        temperature_threshold = int(configs.config.get("watering_temperature_threshold", -1))
        humidity_threshold = int(configs.config.get("watering_humidity_threshold", -1))

        command = "SELECT soil_moisture, air_temperature, air_humidity " \
                  "FROM monitoring " \
                  "ORDER BY id DESC " \
                  "LIMIT {0}".format(num_of_scrapes)

        self.db.query(command)
        monitoring_results = self.db.fetch_result()

        if monitoring_results is not None and len(monitoring_results) >= num_of_scrapes:
            ''' All scrapes should have values above/below threasholds '''

            # logger.info("Checking soil moisture. Threshold value: '{0}'. Last {1} scrapes: {2}".format(moisture_threshold, num_of_scrapes, monitoring_results))

            # check soil moisture
            if moisture_threshold != -1:
                num_accomplished = 0
                for scrape in monitoring_results:
                    if (scrape[0] <= moisture_threshold):
                        num_accomplished += 1
                    else:
                        break

                if num_accomplished != num_of_scrapes:
                    return False

            # check air temperature
            if temperature_threshold != -1:
                num_accomplished = 0
                for scrape in monitoring_results:
                    if (scrape[1] >= temperature_threshold):
                        num_accomplished += 1
                    else:
                        break

                if num_accomplished != num_of_scrapes:
                    return False

            # check air humidity
            if humidity_threshold != -1:
                num_accomplished = 0
                for scrape in monitoring_results:
                    if (scrape[2] <= humidity_threshold):
                        num_accomplished += 1
                    else:
                        break

                if num_accomplished != num_of_scrapes:
                    return False

            return True

    def run(self):
        logger.debug("WateringChecker is preparing")
        time.sleep(3) # wait for 3 seconds for system to get ready
        logger.debug("WateringChecker started.")

        while not self._stop_event.is_set():
            logger.info("Checking for watering conditions...")

            led_on = str_to_bool(configs.config.get("light_on_during_watering"))
            watering_checker_interval = int(str(configs.config.get("watering_checker_interval")))

            if self.should_water():
                logger.debug("Conditions for watering are met.")

                with configs.app.app_context(): # due to 'Working outside of application context.' error
                    self.wateringController.do_watering(led_on = led_on, comment="Watering initiated by the system")

                send_notification = str_to_bool(configs.config.get("telegram_notifier_enabled"))
                if send_notification:
                    TelegramNotifier.getInstance().send_text("Conditions for watering are met. Watering the plant for 3 seconds...")

            time.sleep(watering_checker_interval)

        logger.debug("WateringChecker stopped.")