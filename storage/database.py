from setup import configs

import psycopg2
import logging

logging.basicConfig()
logger = logging.getLogger('database')
logger.setLevel(logging.DEBUG)

DATABASE_HOST = configs.config.get("database_host")
DATABASE_NAME = configs.config.get("database_name")
DATABASE_USER = configs.config.get("database_user")
DATABASE_PASSWORD = configs.config.get("database_password")

class Database():
    __instance = None

    @staticmethod
    def getInstance():
        if Database.__instance == None:
            Database()
        return Database.__instance

    def __initDb(self):
        try:
            self.connection = psycopg2.connect(host=DATABASE_HOST, database=DATABASE_NAME, user=DATABASE_USER, password=DATABASE_PASSWORD)
            self.cursor = self.connection.cursor()
        except (Exception, psycopg2.DatabaseError) as error:
            logger.error(str(error.message))
            if self.connection is not None:
                self.connection.close()

    def __init__(self):
        if Database.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            self.__initDb()
            Database.__instance = self

    def query(self, query):
        self.cursor.execute(query)
        self.connection.commit()

    def fetch_result(self):
        data = self.cursor.fetchall()
        if data is not None:
            return data
        else:
            return []

    def close(self):
        self.cursor.close()
        self.connection.close()
        logging.debug('Database connection is closed')
