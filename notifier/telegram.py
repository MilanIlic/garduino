from setup import configs

import logging
import telebot

TELEGRAM_BOT_TOKEN = configs.config.get("TELEGRAM_BOT_TOKEN")
TELEGRAM_CHAT_ID = int(configs.config.get("TELEGRAM_CHAT_ID"))

logging.basicConfig()
logger = logging.getLogger('telegram-notifier')
logger.setLevel(logging.DEBUG)

class TelegramNotifier:
    __instance = None

    @staticmethod
    def getInstance():
        if TelegramNotifier.__instance == None:
            TelegramNotifier()
        return TelegramNotifier.__instance

    def initNotifier(self):
        self.bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)

    def __init__(self):
        if TelegramNotifier.__instance != None:
            raise Exception("Class is singleton!")
        else:
            self.initNotifier()
            TelegramNotifier.__instance = self

    def send_text(self, message):
        self.bot.send_message(chat_id=TELEGRAM_CHAT_ID, text=message)
        logger.info("Message '{}' sent via Telegram.".format(message))
