from storage.database import Database
from arduino.communication.serial_communicator import SerialCommunicator
from setup import configs

import logging
import time
import threading

logging.basicConfig()
logger = logging.getLogger('monitoring')
logger.setLevel(logging.DEBUG)

class Monitoring (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

        self.arduino = SerialCommunicator.getInstance()
        try:
            self.db = Database.getInstance()
            self.create_table()
        except Exception as e:
            logger.error("Error while connecting to DB: " + str(e))

    def run(self):
        logger.debug("Starting Monitoring")
        self.gather_metrics()
        logger.debug("Exiting Monitoring")

    def gather_metrics(self):
        while True:
            try:
                watering_scrape_interval_sec = int(str(configs.config.get("watering_scrape_interval")))
                time.sleep(watering_scrape_interval_sec)

                # read the latest data from the arduino in format AIR_TEMPERATURE, AIR_HUMIDITY, SOIL_MOISTURE
                data = self.arduino.readline(seek_to_last_received=True)
                logger.debug(data)
                pieces = data.split(" ")  # split the data by the tab
                try:
                    self.db.query("INSERT INTO monitoring (air_temperature, air_humidity, soil_moisture) VALUES ({0},{1},{2})"
                                  .format(pieces[0], pieces[1], pieces[2]))
                except Exception as e:
                    logger.error("Error while inserting data: " + str(e))
            except Exception as e:
                logger.error("Error while reading data: " + str(e))

    def create_table(self):
        ''' Creates monitoring table in the PostgreSQL database '''

        command = "CREATE TABLE IF NOT EXISTS monitoring ( \
                   id SERIAL PRIMARY KEY, \
                   air_temperature numeric(9,2), \
                   air_humidity numeric(9,2), \
                   soil_moisture numeric(9,2), \
                   created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc') \
                   )"

        self.db.query(command)