from setup import configs

import functools
import logging
import serial
import threading

DEVICE = configs.config.get("arduino_serial_port", "/dev/ttyUSB0")

logging.basicConfig()
logger = logging.getLogger('serail-communicator')
logger.setLevel(logging.DEBUG)

def synchronized(wrapped):
    lock = threading.Lock()

    @functools.wraps(wrapped)
    def _wrap(*args, **kwargs):
        with lock:
            return wrapped(*args, **kwargs)
    return _wrap

class SerialCommunicator():
    __instance = None

    @staticmethod
    def getInstance():
        if SerialCommunicator.__instance == None:
            SerialCommunicator()
        return SerialCommunicator.__instance

    def __initSerialPort(self):
        try:
            logger.debug("Trying..." + DEVICE)
            self.arduino = serial.Serial(DEVICE, 9600)
        except:
            logger.error("Failed to connect on " + DEVICE)

    def __init__(self):
        if SerialCommunicator.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            self.__initSerialPort()
            SerialCommunicator.__instance = self

    @synchronized
    def readline(self, seek_to_last_received=False):

        if seek_to_last_received:
            buffer_string = self.arduino.read(self.arduino.inWaiting()).decode() # read all waiting bytes
            if '\n' in buffer_string:
                lines = buffer_string.split('\n')
                return lines[-2] # return last received complete line

        return self.arduino.readline().decode()

    @synchronized
    def write(self, data):
        try:
            logger.debug("Writing '{0}' to Arduino.".format(str(data).encode('utf-8')))
            self.arduino.write(str(data).encode('utf-8'))
        except:
            logger.error("Writing of '{0}' failed.".format(str(data)))

    def __del__(self):
        self.close()

    def close(self):
        self.arduino.close()
        logger.debug('Arduino connection is closed')
