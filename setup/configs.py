from flask import Flask, request
from flask_restful import Resource, Api
import argparse
import yaml
import logging.handlers
import os


help_description = """
(c) 2020, Milan Ilic <ilic.milan1995@gmail.com>

Garduino Web Service.
Options:
  * config_file - Configuration file to load
"""

parser = argparse.ArgumentParser(description=help_description, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-c", "--config_file", help="Configuration file to load.")
ARGS = parser.parse_args()

config = {}

# Load configs
with open(ARGS.config_file, 'r') as file:
   config = yaml.safe_load(file)

app = Flask(__name__)
api = Api(app)

# Logging config
LOG_FILE = config.get("log_file")
LOG_FORMAT='%(asctime)s %(levelname)-8s [%(module)s]: %(message)s'

formatter = logging.Formatter(LOG_FORMAT)

# rotate log file when it reaches out 10 MB
handler = logging.handlers.RotatingFileHandler(
    LOG_FILE, mode='w', maxBytes=10485760, backupCount=5)

handler.setFormatter(formatter)

# Roll over existing file
if os.path.isfile(LOG_FILE):
    handler.doRollover()

logging.basicConfig(level=logging.DEBUG,
                    format=LOG_FORMAT,
                    filename=LOG_FILE,
                    filemode='w')

logger = logging.getLogger('main')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

# Rasspberry Pi - Arduino commands
ARDUINO_WATER=1
ARDUINO_WATER_LED=2
ARDUINO_LED=3
ARDUINO_UPDATE_CONFIG=4
